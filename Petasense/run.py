from app import app
import os
if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    from os import environ
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
