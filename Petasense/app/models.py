from app import db
import datetime
import json

class Location(db.Model):
    __tablename__ = 'locations'

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(256), index=True)


class Machine(db.Model):
    __tablename__ = 'machines'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(60), index=True)
    location_id = db.Column(db.Integer, db.ForeignKey('locations.id'))
    location = db.relationship('Location', backref='machine')
    measurement_loactions = db.relationship('MeasurementLocation', backref='machines')

    def __repr__(self):
        return '<Machine: {}>'.format(self.name)
    
    def latest_measurement_time(self):
        dates = []
        dates.append(datetime.datetime(1901,1,1))
        for ml in self.measurement_loactions:
          for x in ml.vibration_measurements:
            if(x.created_at != None):
              dates.append(x.created_at)
        return max(dates)

    def avarage_measurent(self):
        avg = 0
        count = 1
        for ml in self.measurement_loactions:
          for x in ml.vibration_measurements:
            avg = avg + x.value
            count = count + 1
        return(avg/count)

class MeasurementLocation(db.Model):
    __tablename__ = 'measurement_loactions'

    id = db.Column(db.Integer, primary_key=True)
    machine_id = db.Column(db.Integer, db.ForeignKey('machines.id'))
    machine = db.relationship('Machine', backref='measurement_loaction')
    vibration_measurements = db.relationship('VibrationMeasurement', backref='measurement_loactions')


class VibrationMeasurement(db.Model):
    __tablename__ = 'vibration_measurements'

    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float)
    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    measurement_loaction_id = db.Column(db.Integer, db.ForeignKey('measurement_loactions.id'))
    measurement_loaction = db.relationship('MeasurementLocation', backref='vibration_measurement')

