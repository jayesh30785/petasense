from flask import Flask
from flask_sqlalchemy import SQLAlchemy
#from flask_migrate import Migrate
#Create an Instance of Flask
app = Flask(__name__)
#Include config from config.py
app.config.from_pyfile('config.py')
#app.secret_key = 'some_secret'
#Create an instance of SQLAclhemy
db = SQLAlchemy(app)
from app import models,views

db.create_all()
