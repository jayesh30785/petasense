from flask import render_template, Flask, request, jsonify, json
from models import Machine, Location, MeasurementLocation, VibrationMeasurement, datetime
from app import app
from app import db
import random

@app.route('/')
@app.route('/index.html')
def machines():
  if request.method == 'GET':
    results = Machine.query.all()

    json_results = []
    for result in results:
      d = { 'id': result.id,
           'name': result.name,
           'last_measurement_time': "{:%B %d, %Y %I:%M %p}".format(result.latest_measurement_time()),
           'area': result.location.address,
           'rms': result.avarage_measurent()
           }
      json_results.append(d)

    return render_template('index.html', machines=json_results)
    
@app.route('/machine/<id>')
def machine(id):
  if request.method == 'GET':
    result = Machine.query.get(id)
    if result == None:
      return "No Machine Found"
    machine_details = { 'name': result.name,
           'last_measurement_time': "{:%B %d, %Y %I:%M %p}".format(result.latest_measurement_time()),
           'area': result.location.address,
           'rms': result.avarage_measurent()
           }
    location = []
    for ml in result.measurement_loactions:
        values = []
        for x in ml.vibration_measurements:
            values.append({'value': x.value, 'created_at':str(x.created_at)})
        location.append({ 'id': ml.id,'value': values})
    
    return render_template('machine.html', data = location[0]['value'], machine_details = machine_details)

@app.route('/generate_random_data/<city>')
def generate_random_data(city):
  if request.method == 'GET':
    #return jsonify(random.uniform(1, 3))
    results = Machine.query.all()
    if len(results) >= 10:
      return 'can not generate more data'
#    cities = ['Mumbai', 'Delhi', 'Indore', 'Bangalore', 'Pune', 'Noida', 'Kanpur', 'Agra', 'Bhopal', 'Nagpur']
#    for city in cities:
    l = Location(address=city)
    db.session.add(l)
    m = Machine(name=city+'Machine')
    m.location = l
    db.session.add(m)
    ml = MeasurementLocation()
    ml.machine = m
    db.session.add(m)
    now = datetime.datetime.now() - datetime.timedelta(hours = 800)
    for j in range(800):
      vm = VibrationMeasurement(value=random.uniform(0, 2), created_at = now)
      vm.measurement_loaction = ml
      db.session.add(vm)
      now = now + datetime.timedelta(hours = 1)
    db.session.commit()
    return 'here'
